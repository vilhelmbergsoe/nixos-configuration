{ config, pkgs, ... }:
let

in
  {
    # Enable home-manager
    programs.home-manager.enable = true;

    xdg.userDirs = {
      enable = true;
      documents = "$HOME/docs";
      download = "$HOME/dl";
      videos = "$HOME/vids";
      music = "$HOME/music";
      pictures = "$HOME/pics";
    };

    home.file = {
      ".local/share/dwm/autostart.sh" = {
        executable = true;
        text = "
        #!/bin/sh
        clipmenud &
        dunst &
	slstatus &
        feh --bg-scale $HOME/pix/wallpaper.png
        xrandr --rate 60
      };
    };
    
    home.stateVersion = "21.05";
  }
