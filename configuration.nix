# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
      ./packages/packages.nix
    ];

  nixpkgs.config.allowUnfree = true;

  nixpkgs.overlays = [
    (final: prev: {
    dwm = prev.dwm.overrideAttrs (old: {
      src = pkgs.fetchFromGitHub {
        owner = "vilhelmbergsoe";
        repo = "dwm";
        rev = "bc6f51a7e5bf00b2097456032fb2ac5e88ccb2ef";
        #sha256 = "0000000000000000000000000000000000000000000000000000";
        sha256 = "1s1c122z197gr2y2g35y4w4jsmi6n1dpdwn71s0q1k85hf8sxhnh";
      };
    });
    })
  ];

  # Auto cleanup
  nix = {
    autoOptimiseStore = true;
    gc = {
      automatic = true;
      dates = "daily";
    };
    package = pkgs.nixUnstable;
  };

  # Use the systemd-boot EFI boot loader and clean /tmp on boot
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.cleanTmpDir = true;

  # Set up networking
  networking = {
    hostName = "nixos";
    networkmanager.enable = true;
  };

  # Set up locales (timezone and keyboard layout)
  time.timeZone = "Europe/Copenhagen";
  i18n.defaultLocale = "en_US.UTF-8";
  console = {
    font = "Lat2-Terminus16";
    keyMap = "dk";
  };

  # X settings
  services.xserver = {
    layout = "dk";
    enable = true;
    libinput.enable = true;
    displayManager.lightdm.enable = true;
    windowManager.dwm.enable = true;
  };

  # Enable CUPS to print documents.
  services.printing.enable = true;

  # Enable sound.
  # sound.enable = true;
  # hardware.pulseaudio.enable = true;
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
  };

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.vb = {
    isNormalUser = true;
    extraGroups = [ "wheel" ]; # Enable ‘sudo’ for the user.
    shell = pkgs.bash;
  };

  # Fix Steam
  hardware.opengl.driSupport32Bit = true;

  # Do not touch
  system.stateVersion = "21.05"; # Did you read the comment?

}

