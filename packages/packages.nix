{ pkgs, config, ... }:

let

in

{
  # Install all the packages
  environment.systemPackages = with pkgs; [
    # Desktop
    dwm dmenu feh dunst bash brightnessctl

    (st.overrideAttrs (oldAttrs: rec {
      src = fetchFromGitHub {
        owner = "vilhelmbergsoe";
        repo = "st";
        rev = "a8a692a74d41db87c30f1bb2a1fa2d55c48aed31";
        # sha256 = "0000000000000000000000000000000000000000000000000000";
        sha256 = "11wshzqcdq1za329xln05k2b3c3hxv6w0jn0d87w231crgapzvh4";
      };
    }))
    (slstatus.overrideAttrs (oldAttrs: rec {
      src = fetchFromGitHub {
        owner = "vilhelmbergsoe";
        repo = "slstatus";
        rev = "5a218bc022bcd7c6ae1deb25f20e7963da155aec";
        sha256 = "0000000000000000000000000000000000000000000000000000";
      };
    }))

    # Command-line tools
    fzf ripgrep tree fd
    pass gnupg ffmpeg libnotify tealdeer update-nix-fetchgit
    htop imagemagick

    # GUI applications
    brave mpv sxiv

    # Development
    git gcc gnumake go
  ];
}
